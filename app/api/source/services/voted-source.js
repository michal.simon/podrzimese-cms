const { Types } = require('mongoose');
const { convertRestQueryParams } = require('strapi-utils');

function getCategories(params) {
  if (params.tags_category) {
    const category = params.tags_category;
    return [].concat(category);
  }
}

function getTags(params) {
  if (params.tags) {
    const tags = params.tags;
    return [].concat(tags);
  }
}

function sortNum(field) {
  if (field.order === 'asc') {
    return -1;
  } else {
    return 1;
  }
}

function constructSortObject(queryParams) {
  const sort = { $sort: {} };
  if (queryParams.sort) {
    queryParams.sort.forEach((sortItem) => {
      if (sortItem.field === 'featured') {
        sort.$sort[ sortItem.field ] = sortNum(sortItem);
      } else {
        sort.$sort[ sortItem.field ] = sortNum(sortItem);
      }
    });
  } else {
    sort.$sort._id = 1;
  }
  return sort;
}

function getToday() {
  const today = new Date();
  today.setHours(0, 0, 0, 0);
  return today;
}

function getWeekAgo() {
  const weekAgo = new Date();
  weekAgo.setHours(0, 0, 0, 0);
  weekAgo.setDate(weekAgo.getDate() - 7);
  return weekAgo;
}

function sourceMatchByTags(params) {
  const categories = getCategories(params);
  const tags = getTags(params);

  const filter = {};
  if (categories) {
    filter.$and = [ { tags: { $all: categories.filter(Types.ObjectId.isValid).map(Types.ObjectId) } } ];
  }

  if (tags) {
    filter.tags = {
      $in: tags.filter(Types.ObjectId.isValid).map(Types.ObjectId)
    };
  }

  return {
    $match: filter
  };
}

function sourceMatchById(id) {
  return {
    $match: { _id: Types.ObjectId(id) }
  };
}

async function votedSourceAggPipeline(match, start = 0, limit = 1, sort) {

  const featuredTag = await strapi.models[ 'tag' ].findOne({ text: 'Featured' });

  const today = getToday();
  const weekAgo = getWeekAgo();

  if (!sort) {
    sort = {
      $sort: { _id: 1 }
    };
  }

  return strapi.models[ 'source' ].aggregate([
    match,
    {
      $lookup: {
        from: 'votes',
        localField: '_id',
        foreignField: 'source',
        as: 'votes'
      }
    },
    {
      $addFields: {
        featured: featuredTag ? { $in: [ featuredTag._id, '$tags' ] } : false,
        votesLastWeek: {
          $size: {
            $filter: {
              input: '$votes',
              as: 'vote',
              cond: { $gte: [ '$$vote.createdAt', weekAgo ] }
            }
          }
        },
        votesToday: {
          $size: {
            $filter: {
              input: '$votes',
              as: 'vote',
              cond: { $gte: [ '$$vote.createdAt', today ] }
            }
          }
        },
        votesAll: { $size: '$votes' }
      }
    },
    { $project: { votes: false } },
    sort,
    { $skip: start },
    { $limit: limit }
  ]);
}

module.exports = {
  async findVotedSource(params) {
    const queryParams = convertRestQueryParams(params);
    const sort = constructSortObject(queryParams);
    return votedSourceAggPipeline(sourceMatchByTags(params), queryParams.start, queryParams.limit, sort);
  },
  async countVotedSource(params) {
    const [ result ] = await strapi.models[ 'source' ].aggregate([
      sourceMatchByTags(params),
      { $count: 'count' }
    ]);
    if (!result) {
      return 0;
    }
    return result.count;
  },
  async getVotedSource(id) {
    if (!Types.ObjectId.isValid(id)) {
      return;
    }
    const [ result ] = await votedSourceAggPipeline(sourceMatchById(id));
    return result;
  }
};
